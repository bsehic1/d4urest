package bsehic1.d4urest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@SpringBootApplication
@EnableJpaAuditing
public class D4urestApplication {

    public static void main(String[] args) {
        SpringApplication.run(D4urestApplication.class, args);
    }
}
