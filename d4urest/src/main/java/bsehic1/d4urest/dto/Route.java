package bsehic1.d4urest.dto;

import bsehic1.d4urest.domainModel.RouteModel;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

public class Route implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long routeId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    private Long createdById;

    private Long originId;

    private Long destinationId;

    private String title;

    private String description;

    public Route() {
    }

    public Route(Long routeId, Date created, Date updated, Long createdById, Long originId, Long destinationId, String title, String description) {
        this.routeId = routeId;
        this.created = created;
        this.updated = updated;
        this.createdById = createdById;
        this.originId = originId;
        this.destinationId = destinationId;
        this.title = title;
        this.description = description;
    }

    public Route(RouteModel routeModel){
        this.routeId = routeModel.getRouteId();
        this.created = routeModel.getCreated();
        this.updated = routeModel.getUpdated();
        this.createdById = routeModel.getCreatedById();
        this.originId = routeModel.getOriginId();
        this.destinationId = routeModel.getDestinationId();
        this.title = routeModel.getTitle();
        this.description = routeModel.getDescription();
    }

    public RouteModel toRouteModel(){
        return new RouteModel(this);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getOriginId() {
        return originId;
    }

    public void setOriginId(Long originId) {
        this.originId = originId;
    }

    public Long getDestinationId() {
        return destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
