package bsehic1.d4urest.dto;

import bsehic1.d4urest.domainModel.DriveModel;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

public class Drive implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long driveId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    private Long createdById;

    private Long startLocationId;

    private Long endLocationId;

    private Long routeId;

    private String title;

    private String description;

    private byte poolCapacity;

    public Drive() {
    }

    public Drive(Long driveId, Date created, Date updated, Long createdById, Long startLocationId, Long endLocationId, Long routeId, String title, String description, byte poolCapacity) {
        this.driveId = driveId;
        this.created = created;
        this.updated = updated;
        this.createdById = createdById;
        this.startLocationId = startLocationId;
        this.endLocationId = endLocationId;
        this.routeId = routeId;
        this.title = title;
        this.description = description;
        this.poolCapacity = poolCapacity;
    }

    public Drive(DriveModel driveModel){
        this.driveId = driveModel.getDriveId();
        this.created = driveModel.getCreated();
        this.updated = driveModel.getUpdated();
        this.createdById = driveModel.getCreatedById();
        this.startLocationId = driveModel.getStartLocationId();
        this.endLocationId = driveModel.getEndLocationId();
        this.routeId = driveModel.getRouteId();
        this.title = driveModel.getTitle();
        this.description = driveModel.getDescription();
        this.poolCapacity = driveModel.getPoolCapacity();
    }

    public DriveModel toDriveModel(){
        return new DriveModel(this);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getDriveId() {
        return driveId;
    }

    public void setDriveId(Long driveId) {
        this.driveId = driveId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getStartLocationId() {
        return startLocationId;
    }

    public void setStartLocationId(Long startLocationId) {
        this.startLocationId = startLocationId;
    }

    public Long getEndLocationId() {
        return endLocationId;
    }

    public void setEndLocationId(Long endLocationId) {
        this.endLocationId = endLocationId;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte getPoolCapacity() {
        return poolCapacity;
    }

    public void setPoolCapacity(byte poolCapacity) {
        this.poolCapacity = poolCapacity;
    }
}
