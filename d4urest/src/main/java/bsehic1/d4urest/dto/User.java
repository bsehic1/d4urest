package bsehic1.d4urest.dto;

import bsehic1.d4urest.domainModel.UserModel;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class User {


    private static final long serialVersionUID = 1L;

    private Long userId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    private String email;

    private String passwordHash;

    private String username;

    private String firstName;

    private String lastName;

    private String phoneNumber;

    private String profilePictureUrl;

    public User() {
    }

    public User(Long userId, Date created, Date updated, String email, String passwordHash, String username, String firstName, String lastName, String phoneNumber, String profilePictureUrl) {
        this.userId = userId;
        this.created = created;
        this.updated = updated;
        this.email = email;
        this.passwordHash = passwordHash;
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.profilePictureUrl = profilePictureUrl;
    }

    public User(UserModel userModel){
        this.userId = userModel.getUserId();
        this.created = userModel.getCreated();
        this.updated = userModel.getUpdated();
        this.email = userModel.getEmail();
        this.passwordHash = userModel.getPasswordHash();
        this.username = userModel.getUsername();
        this.firstName = userModel.getFirstName();
        this.lastName = userModel.getLastName();
        this.phoneNumber = userModel.getPhoneNumber();
        this.profilePictureUrl = userModel.getProfilePictureUrl();
    }

    public UserModel toUserModel(){
        return new UserModel(this);
    }
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }
}
