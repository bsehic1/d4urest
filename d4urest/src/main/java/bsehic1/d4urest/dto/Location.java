package bsehic1.d4urest.dto;

import bsehic1.d4urest.domainModel.LocationModel;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

public class Location implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long locationId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    private double longitude;

    private double latitude;

    private String title;

    private Long routeId;

    public Location() {
    }

    public Location(Long locationId, Date created, Date updated, double longitude, double latitude, String title, Long routeId) {
        this.locationId = locationId;
        this.created = created;
        this.updated = updated;
        this.longitude = longitude;
        this.latitude = latitude;
        this.title = title;
        this.routeId = routeId;
    }

    public Location(LocationModel locationModel){
        this.locationId = locationModel.getLocationId();
        this.created = locationModel.getCreated();
        this.updated = locationModel.getUpdated();
        this.latitude = locationModel.getLatitude();
        this.longitude = locationModel.getLongitude();
        this.title = locationModel.getTitle();
        this.routeId = locationModel.getRouteId();
    }

    public LocationModel toLocationModel(){
        return new LocationModel(this);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }
}
