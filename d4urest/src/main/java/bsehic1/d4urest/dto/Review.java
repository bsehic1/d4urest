package bsehic1.d4urest.dto;

import bsehic1.d4urest.domainModel.ReviewModel;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

public class Review {

    private static final long serialVersionUID = 1L;

    private Long reviewId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    private String comment;

    private byte stars;

    protected void onCreate() {
        created = new Date();
    }

    protected void onUpdate() {
        updated = new Date();
    }

    private Long reviewerId;

    private Long revieweeId;

    public Review() {
    }

    public Review(Long reviewId, Date created, Date updated, String comment, byte stars, Long reviewerId, Long revieweeId) {
        this.reviewId = reviewId;
        this.created = created;
        this.updated = updated;
        this.comment = comment;
        this.stars = stars;
        this.reviewerId = reviewerId;
        this.revieweeId = revieweeId;
    }

    public Review(ReviewModel reviewModel){
        this.reviewId = reviewModel.getReviewId();
        this.created = reviewModel.getCreated();
        this.updated = reviewModel.getUpdated();
        this.comment = reviewModel.getComment();
        this.stars = reviewModel.getStars();
        this.reviewerId = reviewModel.getReviewerId();
        this.revieweeId = reviewModel.getRevieweeId();
    }

    public ReviewModel toReviewModel(){
        return new ReviewModel(this);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public byte getStars() {
        return stars;
    }

    public void setStars(byte stars) {
        this.stars = stars;
    }

    public Long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(Long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public Long getRevieweeId() {
        return revieweeId;
    }

    public void setRevieweeId(Long revieweeId) {
        this.revieweeId = revieweeId;
    }
}
