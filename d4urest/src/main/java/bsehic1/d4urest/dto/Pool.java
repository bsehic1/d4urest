package bsehic1.d4urest.dto;

import bsehic1.d4urest.domainModel.PoolModel;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

public class Pool implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long poolId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    private Long createdById;

    private Long passengerId;

    private Long driveId;

    public Pool() {
    }

    public Pool(Long poolId, Date created, Date updated, Long createdById, Long passengerId, Long driveId) {
        this.poolId = poolId;
        this.created = created;
        this.updated = updated;
        this.createdById = createdById;
        this.passengerId = passengerId;
        this.driveId = driveId;
    }

    public Pool(PoolModel poolModel){
        this.poolId = poolModel.getPoolId();
        this.created = poolModel.getCreated();
        this.updated = poolModel.getUpdated();
        this.createdById = poolModel.getCreatedById();
        this.passengerId = poolModel.getPassengerId();
        this.driveId = poolModel.getDriveId();
    }

    public PoolModel toPoolModel(){
        return new PoolModel(this);
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getPoolId() {
        return poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Long getCreatedById() {
        return createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public Long getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(Long passengerId) {
        this.passengerId = passengerId;
    }

    public Long getDriveId() {
        return driveId;
    }

    public void setDriveId(Long driveId) {
        this.driveId = driveId;
    }
}
