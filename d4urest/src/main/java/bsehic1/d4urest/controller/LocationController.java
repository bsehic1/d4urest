package bsehic1.d4urest.controller;

import bsehic1.d4urest.dto.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import bsehic1.d4urest.service.LocationService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/api/locations")
public class LocationController {

    @Autowired
    private LocationService locationService;

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/route/{id}")
    public List<Location> getLocationsByRoute(@PathVariable(value = "id") Long id){
        return locationService.getLocationsByRouteId(id);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{id}")
    public ResponseEntity<Location> getLocation(@PathVariable(value = "id") Long id){
        Location location = locationService.getLocationById(id);
        if(location == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(location);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/")
    public Location createLocation(@Valid @RequestBody Location location){
        return locationService.createLocation(location);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/{id}")
    public ResponseEntity<Location> updateLocation(@PathVariable(value = "id") Long id, @Valid @RequestBody Location location){
        Location oldLocation = locationService.getLocationById(id);
        if(oldLocation == null){
            return ResponseEntity.notFound().build();
        }
        oldLocation = location;
        locationService.updateLocation(oldLocation);
        return ResponseEntity.ok(oldLocation);
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/{id}")
    public ResponseEntity<Location> deleteLocation(@PathVariable(value = "id") Long id){
        Location location =locationService.getLocationById(id);
        if(location == null){
            return ResponseEntity.notFound().build();
        }
        locationService.deleteLocation(id);
        return ResponseEntity.ok().build();
    }
}
