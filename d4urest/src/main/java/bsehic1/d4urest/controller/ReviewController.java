package bsehic1.d4urest.controller;

import bsehic1.d4urest.dto.Review;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import bsehic1.d4urest.service.ReviewService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/api/reviews")
public class ReviewController {

    @Autowired
    private ReviewService reviewService;

    @GetMapping("/reviewer/{id}")
    public List<Review> getReviewsByReviewer(@PathVariable(value = "id") Long id){
        return reviewService.getReviewsByReviewerId(id);
    }

    @GetMapping("/reviewee/{id}")
    public List<Review> getReviewsByReviewee(@PathVariable(value = "id") Long id){
        return reviewService.getReviewsByRevieweeId(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Review> getReview(@PathVariable(value = "id") Long id){
        Review location = reviewService.getReviewById(id);
        if(location == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(location);
    }

    @PostMapping("/")
    public Review createReview(@Valid @RequestBody Review review){
        return reviewService.createReview(review);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Review> updateReview(@PathVariable(value = "id") Long id, @Valid @RequestBody Review review){
        Review oldReview = reviewService.getReviewById(id);
        if(oldReview == null){
            return ResponseEntity.notFound().build();
        }
        oldReview = review;
        reviewService.updateReview(oldReview);
        return ResponseEntity.ok(oldReview);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Review> deleteReview(@PathVariable(value = "id") Long id){
        Review review =reviewService.getReviewById(id);
        if(review == null){
            return ResponseEntity.notFound().build();
        }
        reviewService.deleteReview(id);
        return ResponseEntity.ok().build();
    }
}
