package bsehic1.d4urest.controller;

import bsehic1.d4urest.dto.Pool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import bsehic1.d4urest.service.PoolService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/api/pools")
public class PoolController {

    @Autowired
    private PoolService poolService;

    @GetMapping("/drive/{id}")
    public List<Pool> getPoolsByDriveId(@PathVariable(value = "id") Long id){
        return poolService.getPoolsByDriveId(id);
    }

    @GetMapping("/createdby/{id}")
    public List<Pool> getPoolsByCreatedById(@PathVariable(value = "id") Long id){
        return poolService.getPoolsByCreatedById(id);
    }

    @GetMapping("/passenger/{id}")
    public List<Pool> getPoolsByPassengerId(@PathVariable(value = "id") Long id){
        return poolService.getPoolsByPassengerId(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Pool> getPoolById(@PathVariable(value = "id") Long id){
        Pool pool = poolService.getPoolById(id);
        if(pool == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(pool);
    }

    @PostMapping("/")
    public Pool createPool(@Valid @RequestBody Pool pool){
        return poolService.createPool(pool);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Pool> updatePool(@PathVariable(value = "id") Long id, @Valid @RequestBody Pool pool){
        Pool oldPool = poolService.getPoolById(id);
        if(oldPool == null){
            return ResponseEntity.notFound().build();
        }
        oldPool = pool;
        poolService.updatePool(oldPool);
        return ResponseEntity.ok(oldPool);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Pool> deletePool(@PathVariable(value = "id") Long id){
        Pool pool = poolService.getPoolById(id);
        if(pool == null){
            return ResponseEntity.notFound().build();
        }
        poolService.deletePool(id);
        return ResponseEntity.ok().build();
    }
}
