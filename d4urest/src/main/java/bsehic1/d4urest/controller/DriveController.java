package bsehic1.d4urest.controller;

import bsehic1.d4urest.dto.Drive;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import bsehic1.d4urest.domainModel.DriveModel;
import bsehic1.d4urest.service.DriveService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/api/drives")
public class DriveController {

    @Autowired
    private DriveService driveService;

    @GetMapping("/route/{id}")
    public List<Drive> getDrivesByRoute(@PathVariable(value = "id") Long id){
        return driveService.getDrivesByRouteId(id);
    }

    @GetMapping("/createdby/{id}")
    public List<Drive> getDrivesByCreatedById(@PathVariable(value = "id") Long id){
        return driveService.getDrivesByCreatedById(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Drive> getDrive(@PathVariable(value = "id") Long id){
        Drive drive = driveService.getDriveById(id);
        if(drive == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(drive);
    }

    @PostMapping("/")
    public Drive createDrive(@Valid @RequestBody Drive drive){
        return driveService.createDrive(drive);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Drive> updateDrive(@PathVariable(value = "id") Long id, @Valid @RequestBody Drive drive){
        Drive oldDrive = driveService.getDriveById(id);
        if(oldDrive == null){
            return ResponseEntity.notFound().build();
        }
        oldDrive = drive;
        driveService.updateDrive(oldDrive);
        return ResponseEntity.ok(oldDrive);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Drive> deleteDrive(@PathVariable(value = "id") Long id){
        Drive drive = driveService.getDriveById(id);
        if(drive == null){
            return ResponseEntity.notFound().build();
        }
        driveService.deleteDrive(id);
        return ResponseEntity.ok().build();
    }
}
