package bsehic1.d4urest.controller;

import bsehic1.d4urest.dto.Route;
import bsehic1.d4urest.service.RouteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/api/routes")
public class RouteController {

    @Autowired
    private RouteService routeService;
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/user/{id}")
    public List<Route> getRoutesByUser(@PathVariable(value = "id") Long id){
        return routeService.getRoutesByUserId(id);
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/all")
    public List<Route> getAllRoutes(){
        return routeService.getAllRoutes();
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @GetMapping("/{id}")
    public ResponseEntity<Route> getRoute(@PathVariable(value = "id") Long id){
        Route route = routeService.getRouteById(id);
        if(route == null){
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().body(route);
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PostMapping("/")
    public Route createRoute(@Valid @RequestBody Route route){

        return routeService.createRoute(route);
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @PutMapping("/{id}")
    public ResponseEntity<Route> updateRoute(@PathVariable(value = "id") Long id, @Valid @RequestBody Route route){
        Route oldRoute = routeService.getRouteById(id);
        if(oldRoute == null){
            return ResponseEntity.notFound().build();
        }
        oldRoute = route;
        routeService.updateRoute(oldRoute);
        return ResponseEntity.ok(oldRoute);
    }
    @CrossOrigin(origins = "http://localhost:3000")
    @DeleteMapping("/{id}")
    public ResponseEntity<Route> deleteRoute(@PathVariable(value = "id") Long id){
        Route route =routeService.getRouteById(id);
        if(route == null){
            return ResponseEntity.notFound().build();
        }
        routeService.deleteRoute(id);
        return ResponseEntity.ok().build();
    }

}
