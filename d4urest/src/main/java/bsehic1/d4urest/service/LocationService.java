package bsehic1.d4urest.service;

import bsehic1.d4urest.domainModel.LocationModel;
import bsehic1.d4urest.dto.Location;

import java.util.List;

public interface LocationService {
    Location createLocation(Location location);
    Location updateLocation(Location location);
    Location getLocationById(Long id);
    void deleteLocation(Long id);
    List<Location> getLocationsByRouteId(Long id);
}
