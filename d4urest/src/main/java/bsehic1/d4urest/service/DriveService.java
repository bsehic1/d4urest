package bsehic1.d4urest.service;

import bsehic1.d4urest.domainModel.DriveModel;
import bsehic1.d4urest.dto.Drive;

import java.util.List;

public interface DriveService {
    Drive createDrive(Drive drive);
    Drive updateDrive(Drive drive);
    Drive getDriveById(Long id);
    void deleteDrive(Long id);
    List<Drive> getDrivesByRouteId(Long id);
    List<Drive> getDrivesByCreatedById(Long id);
}
