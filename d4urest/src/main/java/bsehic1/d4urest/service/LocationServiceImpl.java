package bsehic1.d4urest.service;

import bsehic1.d4urest.domainModel.LocationModel;
import bsehic1.d4urest.dto.Location;
import bsehic1.d4urest.repository.RouteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bsehic1.d4urest.repository.LocationRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class LocationServiceImpl implements LocationService {

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Override
    public Location createLocation(Location location) {
        try {
            LocationModel locationModel = new LocationModel(location);
            if(location.getRouteId() != null){
                locationModel.setRoute(routeRepository.getOne(locationModel.getRouteId()));
            }
            return locationRepository.save(locationModel).toLocation();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Location updateLocation(Location location) {
        try {
            LocationModel locationModel = new LocationModel(location);
            if(location.getRouteId() != null){
                locationModel.setRoute(routeRepository.getOne(locationModel.getRouteId()));
            }
            return locationRepository.save(locationModel).toLocation();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Location getLocationById(Long id) {
        try {
            System.out.println("LocationServiceImpl: " + id);
            return locationRepository.getOne(id).toLocation();
        } catch (Exception e) {
            System.out.println("Error LocationServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteLocation(Long id) {
        try {
            System.out.println("LocationServiceImpl: " + id);
            locationRepository.deleteById(id);

        } catch (Exception e) {
            System.out.println("Error LocationServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<Location> getLocationsByRouteId(Long id) {
        return locationRepository.getLocationsByRouteId(id)
                .stream()
                .map(LocationModel::toLocation)
                .collect(Collectors.toList());
    }

}
