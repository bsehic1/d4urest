package bsehic1.d4urest.service;

import java.util.List;

import bsehic1.d4urest.domainModel.ReviewModel;
import bsehic1.d4urest.dto.Review;

public interface ReviewService {

    Review createReview(Review review);
    Review updateReview(Review review);
    Review getReviewById(Long id);
    void deleteReview(Long id);
    List<Review> getReviewsByReviewerId(Long id);
    List<Review> getReviewsByRevieweeId(Long id);
}
