package bsehic1.d4urest.service;
import bsehic1.d4urest.domainModel.UserModel;
import bsehic1.d4urest.dto.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import bsehic1.d4urest.repository.UserRepository;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User user) {
        try {
            UserModel userModel = new UserModel(user);
            return userRepository.save(userModel).toUser();
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public User updateUser(User user) {
        try {
            UserModel userModel = new UserModel(user);
            return userRepository.save(userModel).toUser();
        } catch (Exception e){
            return null;
        }
    }

    @Override
    public User getUserById(Long id) {
        try {
            System.out.println("UserServiceImpl: " + id);
            return userRepository.getOne(id).toUser();
        } catch (Exception e) {
            System.out.println("Error UserServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteUser(Long id) {
        try {
            System.out.println("UserServiceImpl: " + id);
            userRepository.deleteById(id);

        } catch (Exception e) {
            System.out.println("Error UserServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll()
                .stream()
                .map(UserModel::toUser)
                .collect(Collectors.toList());
    }

    @Override
    public List<User> getPassengersByDriveId(Long id){
        return userRepository.getPassengersByDriveId(id)
                .stream()
                .map(UserModel::toUser)
                .collect(Collectors.toList());
    }
}
