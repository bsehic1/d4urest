package bsehic1.d4urest.service;

import bsehic1.d4urest.dto.Drive;
import bsehic1.d4urest.repository.LocationRepository;
import bsehic1.d4urest.repository.RouteRepository;
import bsehic1.d4urest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bsehic1.d4urest.domainModel.DriveModel;
import bsehic1.d4urest.repository.DriveRepository;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class DriveServiceImpl implements DriveService {

    @Autowired
    private DriveRepository driveRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Override
    public Drive createDrive(Drive drive) {
        try{
            DriveModel driveModel = new DriveModel(drive);
            if(drive.getCreatedById() != null){
                driveModel.setCreatedBy(userRepository.getOne(driveModel.getCreatedById()));
            }
            if(drive.getRouteId() != null){
                driveModel.setRoute(routeRepository.getOne(driveModel.getRouteId()));
            }
            if(drive.getStartLocationId() != null){
                driveModel.setStartLocation(locationRepository.getOne(driveModel.getStartLocationId()));
            }
            if(drive.getEndLocationId() != null){
                driveModel.setEndLocation(locationRepository.getOne(driveModel.getEndLocationId()));
            }
            return driveRepository.save(driveModel).toDrive();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Drive updateDrive(Drive drive) {
        try{
            DriveModel driveModel = new DriveModel(drive);
            if(drive.getCreatedById() != null){
                driveModel.setCreatedBy(userRepository.getOne(driveModel.getCreatedById()));
            }
            if(drive.getRouteId() != null){
                driveModel.setRoute(routeRepository.getOne(driveModel.getRouteId()));
            }
            if(drive.getStartLocationId() != null){
                driveModel.setStartLocation(locationRepository.getOne(driveModel.getStartLocationId()));
            }
            if(drive.getEndLocationId() != null){
                driveModel.setEndLocation(locationRepository.getOne(driveModel.getEndLocationId()));
            }
            return driveRepository.save(driveModel).toDrive();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Drive getDriveById(Long id) {
        try {
            System.out.println("DriveServiceImpl: " + id);
            return driveRepository.getOne(id).toDrive();
        } catch (Exception e) {
            System.out.println("Error DriveServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteDrive(Long id) {
        try {
            System.out.println("DriveServiceImpl: " + id);
            driveRepository.deleteById(id);

        } catch (Exception e) {
            System.out.println("Error DriveServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<Drive> getDrivesByRouteId(Long id) {
        return driveRepository.getDrivesByRouteId(id)
                .stream()
                .map(DriveModel::toDrive)
                .collect(Collectors.toList());
    }

    @Override
    public List<Drive> getDrivesByCreatedById(Long id){
        return driveRepository.getDrivesByCreatedById(id)
                .stream()
                .map(DriveModel::toDrive)
                .collect(Collectors.toList());
    }

}
