package bsehic1.d4urest.service;

import java.util.List;

import bsehic1.d4urest.domainModel.RouteModel;
import bsehic1.d4urest.dto.Route;

public interface RouteService {
    Route createRoute(Route route);
    Route updateRoute(Route route);
    Route getRouteById(Long id);
    void deleteRoute(Long id);
    List<Route> getRoutesByUserId(Long id);
    List<Route> getAllRoutes();
}
