package bsehic1.d4urest.service;

import bsehic1.d4urest.dto.Pool;
import bsehic1.d4urest.repository.DriveRepository;
import bsehic1.d4urest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bsehic1.d4urest.domainModel.PoolModel;
import bsehic1.d4urest.repository.PoolRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class PoolServiceImpl implements PoolService {

    @Autowired
    private PoolRepository poolRepository;

    @Autowired
    private DriveRepository driveRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Pool createPool(Pool pool) {
        try{
            PoolModel poolModel = new PoolModel(pool);
            if(pool.getDriveId() != null){
                poolModel.setDrive(driveRepository.getOne(poolModel.getDriveId()));
            }
            if(pool.getCreatedById() != null){
                poolModel.setCreatedBy(userRepository.getOne(poolModel.getCreatedById()));
            }
            if(pool.getPassengerId() != null){
                poolModel.setPassenger(userRepository.getOne(poolModel.getPassengerId()));
            }
            return poolModel.toPool();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Pool updatePool(Pool pool) {
        try{
            PoolModel poolModel = new PoolModel(pool);
            if(pool.getDriveId() != null){
                poolModel.setDrive(driveRepository.getOne(poolModel.getDriveId()));
            }
            if(pool.getCreatedById() != null){
                poolModel.setCreatedBy(userRepository.getOne(poolModel.getCreatedById()));
            }
            if(pool.getPassengerId() != null){
                poolModel.setPassenger(userRepository.getOne(poolModel.getPassengerId()));
            }
            return poolModel.toPool();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Pool getPoolById(Long id) {
        try {
            System.out.println("PoolServiceImpl: " + id);
            return poolRepository.getOne(id).toPool();
        } catch (Exception e) {
            System.out.println("Error PoolServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deletePool(Long id) {
        try {
            System.out.println("PoolServiceImpl: " + id);
            poolRepository.deleteById(id);

        } catch (Exception e) {
            System.out.println("Error PoolServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<Pool> getPoolsByDriveId(Long id) {
        return poolRepository.getPoolsByDriveId(id)
                .stream()
                .map(PoolModel::toPool)
                .collect(Collectors.toList());
    }

    @Override
    public List<Pool> getPoolsByCreatedById(Long id) {
        return poolRepository.getPoolsByCreatedById(id)
                .stream()
                .map(PoolModel::toPool)
                .collect(Collectors.toList());
    }

    @Override
    public List<Pool> getPoolsByPassengerId(Long id) {
        return poolRepository.getPoolsByPassengerId(id)
                .stream()
                .map(PoolModel::toPool)
                .collect(Collectors.toList());
    }

}
