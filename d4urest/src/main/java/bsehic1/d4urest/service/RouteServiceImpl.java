package bsehic1.d4urest.service;

import bsehic1.d4urest.dto.Route;
import bsehic1.d4urest.repository.LocationRepository;
import bsehic1.d4urest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bsehic1.d4urest.domainModel.RouteModel;
import bsehic1.d4urest.repository.RouteRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RouteServiceImpl implements RouteService {

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private LocationRepository locationRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Route createRoute(Route route) {
        try{
            RouteModel routeModel = new RouteModel(route);
            if(route.getOriginId() != null) {
                routeModel.setOrigin(locationRepository.getOne(route.getOriginId()));
            }
            if(route.getDestinationId() != null) {
                routeModel.setDestination(locationRepository.getOne(route.getDestinationId()));
            }
            if(route.getCreatedById() != null){
                routeModel.setCreatedBy(userRepository.getOne(route.getCreatedById()));
            }
            return routeRepository.save(routeModel).toRoute();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Route updateRoute(Route route) {
        try{
            RouteModel routeModel = new RouteModel(route);
            if(route.getOriginId() != null) {
                routeModel.setOrigin(locationRepository.getOne(route.getOriginId()));
            }
            if(route.getDestinationId() != null) {
                routeModel.setDestination(locationRepository.getOne(route.getDestinationId()));
            }
            if(route.getCreatedById() != null){
                routeModel.setCreatedBy(userRepository.getOne(route.getCreatedById()));
            }
            return routeRepository.save(routeModel).toRoute();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Route getRouteById(Long id) {
        try {
            System.out.println("RouteServiceImpl: " + id);
            return routeRepository.getOne(id).toRoute();
        } catch (Exception e) {
            System.out.println("Error RouteServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteRoute(Long id) {
        try {
            System.out.println("RouteServiceImpl: " + id);
            routeRepository.deleteById(id);

        } catch (Exception e) {
            System.out.println("Error RouteServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<Route> getRoutesByUserId(Long id) {
        return routeRepository.getRoutesByCreatedById(id)
                .stream()
                .map(RouteModel::toRoute)
                .collect(Collectors.toList());
    }

    @Override
    public List<Route> getAllRoutes(){
        return routeRepository.getAllRoutes()
                .stream()
                .map(RouteModel::toRoute)
                .collect(Collectors.toList());
    }
}
