package bsehic1.d4urest.service;

import bsehic1.d4urest.dto.Review;
import bsehic1.d4urest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bsehic1.d4urest.domainModel.ReviewModel;
import bsehic1.d4urest.repository.ReviewRepository;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    private ReviewRepository reviewRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public Review createReview(Review review) {
        try{
            ReviewModel reviewModel = new ReviewModel(review);
            if(review.getReviewerId() != null){
                reviewModel.setReviewer(userRepository.getOne(reviewModel.getReviewerId()));
            }
            if(review.getRevieweeId() != null){
                reviewModel.setReviewee(userRepository.getOne(reviewModel.getRevieweeId()));
            }
            return reviewRepository.save(reviewModel).toReview();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Review updateReview(Review review) {
        try{
            ReviewModel reviewModel = new ReviewModel(review);
            if(review.getReviewerId() != null){
                reviewModel.setReviewer(userRepository.getOne(reviewModel.getReviewerId()));
            }
            if(review.getRevieweeId() != null){
                reviewModel.setReviewee(userRepository.getOne(reviewModel.getRevieweeId()));
            }
            return reviewRepository.save(reviewModel).toReview();
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Review getReviewById(Long id) {
        try {
            System.out.println("ReviewServiceImpl: " + id);
            ReviewModel reviewModel = reviewRepository.getOne(id);
            return reviewModel.toReview();
        } catch (Exception e) {
            System.out.println("Error ReviewServiceImpl: " + e.toString());
            return null;
        }
    }

    @Override
    public void deleteReview(Long id) {
        try {
            System.out.println("ReviewServiceImpl: " + id);
            reviewRepository.deleteById(id);

        } catch (Exception e) {
            System.out.println("Error ReviewServiceImpl: " + e.toString());
        }
    }

    @Override
    public List<Review> getReviewsByReviewerId(Long id) {
        return reviewRepository.getReviewsByReviewerId(id)
                .stream()
                .map(ReviewModel::toReview)
                .collect(Collectors.toList());
    }

    @Override
    public List<Review> getReviewsByRevieweeId(Long id) {
        return reviewRepository.getReviewsByRevieweeId(id)
                .stream()
                .map(ReviewModel::toReview)
                .collect(Collectors.toList());
    }
}
