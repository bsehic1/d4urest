package bsehic1.d4urest.service;

import bsehic1.d4urest.domainModel.PoolModel;
import bsehic1.d4urest.dto.Pool;

import java.util.List;

public interface PoolService {
    Pool createPool(Pool pool);
    Pool updatePool(Pool pool);
    Pool getPoolById(Long id);
    void deletePool(Long id);
    List<Pool> getPoolsByDriveId(Long id);
    List<Pool> getPoolsByCreatedById(Long id);
    List<Pool> getPoolsByPassengerId(Long id);
}
