package bsehic1.d4urest.service;

import java.util.List;

import bsehic1.d4urest.domainModel.UserModel;
import bsehic1.d4urest.dto.User;

public interface UserService {

    User createUser(User user);
    User updateUser(User user);
    User getUserById(Long id);
    void deleteUser(Long id);
    List<User> getAllUsers();
    List<User> getPassengersByDriveId(Long id);

}
