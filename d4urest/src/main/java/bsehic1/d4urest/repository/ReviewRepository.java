package bsehic1.d4urest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bsehic1.d4urest.domainModel.ReviewModel;

import java.util.List;

@Repository
public interface ReviewRepository extends JpaRepository<ReviewModel,Long>{
    List<ReviewModel> getReviewsByRevieweeId(Long id);
    List<ReviewModel> getReviewsByReviewerId(Long id);
    void deleteById(Long id);
}
