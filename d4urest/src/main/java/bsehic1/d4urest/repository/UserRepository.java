package bsehic1.d4urest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import bsehic1.d4urest.domainModel.*;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<UserModel,Long> {
    void deleteById(Long id);

    @Query("SELECT u from UserModel u, DriveModel d, PoolModel p where d.driveId = :id and d.driveId = p.driveId and u.userId = p.passengerId")
    List<UserModel> getPassengersByDriveId(Long id);
}
