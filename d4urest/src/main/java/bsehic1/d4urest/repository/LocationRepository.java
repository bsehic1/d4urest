package bsehic1.d4urest.repository;

import bsehic1.d4urest.domainModel.LocationModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LocationRepository extends JpaRepository<LocationModel,Long> {
    List<LocationModel> getLocationsByRouteId(Long id);
    void deleteById(Long id);
}
