package bsehic1.d4urest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import bsehic1.d4urest.domainModel.RouteModel;

import java.util.List;

@Repository
public interface RouteRepository extends JpaRepository<RouteModel,Long>{
    List<RouteModel> getRoutesByCreatedById(Long id);
    void deleteById(Long id);
    @Query("select r from RouteModel r")
    List<RouteModel> getAllRoutes();
}
