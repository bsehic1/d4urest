package bsehic1.d4urest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bsehic1.d4urest.domainModel.DriveModel;

import java.util.List;

@Repository
public interface DriveRepository extends JpaRepository<DriveModel,Long> {
    List<DriveModel> getDrivesByRouteId(Long id);
    List<DriveModel> getDrivesByCreatedById(Long id);
    void deleteById(Long id);
}