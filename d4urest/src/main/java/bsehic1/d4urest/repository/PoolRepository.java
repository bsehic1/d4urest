package bsehic1.d4urest.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import bsehic1.d4urest.domainModel.PoolModel;

@Repository
public interface PoolRepository extends JpaRepository<PoolModel,Long> {
    void deleteById(Long id);
    List<PoolModel> getPoolsByDriveId(Long id);
    List<PoolModel> getPoolsByCreatedById(Long id);
    List<PoolModel> getPoolsByPassengerId(Long id);
}