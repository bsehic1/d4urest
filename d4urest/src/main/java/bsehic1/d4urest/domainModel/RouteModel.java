package bsehic1.d4urest.domainModel;

import bsehic1.d4urest.dto.Route;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "routes")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) //Serialization happens before lazy loaded objects are loaded
public class RouteModel implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long routeId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = UserModel.class)
    @JoinColumn(name = "createdById")
    private UserModel createdBy;

    @Column(name = "createdById", insertable = false, updatable = false)
    private Long createdById;

    @OneToOne
    @JoinColumn(name = "originId")
    private LocationModel origin;

    @Column(name = "originId", insertable = false, updatable = false)
    private Long originId;

    @OneToOne
    @JoinColumn(name = "destinationId")
    private LocationModel destination;

    @Column(name = "destinationId", insertable = false, updatable = false)
    private Long destinationId;

    private String title;

    private String description;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public RouteModel() {
    }

    public RouteModel(Long routeId, Date created, Date updated, Long createdById, Long originId, Long destinationId, String title, String description) {
        this.routeId = routeId;
        this.created = created;
        this.updated = updated;
        this.createdById = createdById;
        this.originId = originId;
        this.destinationId = destinationId;
        this.title = title;
        this.description = description;
    }

    public RouteModel(Route route){
        this.routeId = route.getRouteId();
        this.created = route.getCreated();
        this.updated = route.getUpdated();
        this.createdById = route.getCreatedById();
        this.originId = route.getOriginId();
        this.destinationId = route.getDestinationId();
        this.title = route.getTitle();
        this.description = route.getDescription();
    }

    public Route toRoute(){
        return new Route(this);
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UserModel getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserModel createdBy) {
        this.createdBy = createdBy;
    }


    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public Long getCreatedById() {
        return this.createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public LocationModel getOrigin() {
        return this.origin;
    }

    public void setOrigin(LocationModel origin) {
        this.origin = origin;
    }

    public Long getOriginId() {
        return this.originId;
    }

    public void setOriginId(Long originId) {
        this.originId = originId;
    }

    public LocationModel getDestination() {
        return this.destination;
    }

    public void setDestination(LocationModel destination) {
        this.destination = destination;
    }

    public Long getDestinationId() {
        return this.destinationId;
    }

    public void setDestinationId(Long destinationId) {
        this.destinationId = destinationId;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
