package bsehic1.d4urest.domainModel;

import bsehic1.d4urest.dto.Drive;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "drives")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) //Serialization happens before lazy loaded objects are loaded
public class DriveModel implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long driveId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = UserModel.class)
    @JoinColumn(name = "createdById")
    private UserModel createdBy;

    @Column(name = "createdById", insertable = false, updatable = false)
    private Long createdById;

    @OneToOne(fetch = FetchType.EAGER, targetEntity = LocationModel.class)
    @JoinColumn(name = "startLocationId")
    private LocationModel startLocation;

    @Column(name = "startLocationId", insertable = false, updatable = false)
    private Long startLocationId;

    @OneToOne(fetch = FetchType.EAGER, targetEntity = LocationModel.class)
    @JoinColumn(name = "endLocationId")
    private LocationModel endLocation;

    @Column(name = "endLocationId", insertable = false, updatable = false)
    private Long endLocationId;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = RouteModel.class)
    @JoinColumn(name = "routeId")
    private RouteModel route;

    @Column(name = "routeId", insertable = false, updatable = false)
    private Long routeId;

    private String title;

    private String description;

    private byte poolCapacity;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public DriveModel() {
    }

    public DriveModel(Drive drive){
        this.driveId = drive.getDriveId();
        this.createdById = drive.getCreatedById();
        this.startLocationId = drive.getStartLocationId();
        this.endLocationId = drive.getEndLocationId();
        this.routeId = drive.getRouteId();
        this.title = drive.getTitle();
        this.description = drive.getDescription();
        this.poolCapacity = drive.getPoolCapacity();
    }

    public DriveModel(Long driveId, Long createdById, Long startLocationId, Long endLocationId, Long routeId, String title, String description, byte poolCapacity) {
        this.driveId = driveId;
        this.createdById = createdById;
        this.startLocationId = startLocationId;
        this.endLocationId = endLocationId;
        this.routeId = routeId;
        this.title = title;
        this.description = description;
        this.poolCapacity = poolCapacity;
    }

    public Drive toDrive(){
        return new Drive(this);
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public UserModel getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserModel createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreated() {
        return this.created;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public Long getCreatedById() {
        return this.createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public LocationModel getStartLocation() {
        return this.startLocation;
    }

    public void setStartLocation(LocationModel startLocation) {
        this.startLocation = startLocation;
    }

    public Long getStartLocationId() {
        return this.startLocationId;
    }

    public void setStartLocationId(Long startLocationId) {
        this.startLocationId = startLocationId;
    }

    public LocationModel getEndLocation() {
        return this.endLocation;
    }

    public void setEndLocation(LocationModel endLocation) {
        this.endLocation = endLocation;
    }

    public Long getEndLocationId() {
        return this.endLocationId;
    }

    public void setEndLocationId(Long endLocationId) {
        this.endLocationId = endLocationId;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getDriveId() {
        return this.driveId;
    }

    public void setDriveId(Long driveId) {
        this.driveId = driveId;
    }

    public RouteModel getRoute() {
        return this.route;
    }

    public void setRoute(RouteModel route) {
        this.route = route;
    }

    public Long getRouteId() {
        return this.routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public byte getPoolCapacity() {
        return this.poolCapacity;
    }

    public void setPoolCapacity(byte poolCapacity) {
        this.poolCapacity = poolCapacity;
    }

}
