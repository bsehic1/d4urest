package bsehic1.d4urest.domainModel;

import bsehic1.d4urest.dto.Review;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "reviews")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) //Serialization happens before lazy loaded objects are loaded
public class ReviewModel implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long reviewId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    private String comment;

    private byte stars;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = UserModel.class)
    @JoinColumn(name = "reviewerId")
    private UserModel reviewer;

    @Column(name = "reviewerId", updatable = false, insertable = false)
    private Long reviewerId;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = UserModel.class)
    @JoinColumn(name = "revieweeId")
    private UserModel reviewee;

    @Column(name = "revieweeId", updatable = false, insertable = false)
    private Long revieweeId;

    public ReviewModel() {
    }

    public ReviewModel(Long reviewId, Date created, Date updated, String comment, byte stars, Long reviewerId, Long revieweeId) {
        this.reviewId = reviewId;
        this.created = created;
        this.updated = updated;
        this.comment = comment;
        this.stars = stars;
        this.reviewerId = reviewerId;
        this.revieweeId = revieweeId;
    }

    public ReviewModel(Review review){
        this.reviewId = review.getReviewId();
        this.created = review.getCreated();
        this.updated = review.getUpdated();
        this.comment = review.getComment();
        this.stars = review.getStars();
        this.reviewerId = review.getReviewerId();
        this.revieweeId = review.getRevieweeId();
    }

    public Review toReview(){
        return new Review(this);
    }

    public Long getReviewerId() {
        return reviewerId;
    }

    public void setReviewerId(Long reviewerId) {
        this.reviewerId = reviewerId;
    }

    public Long getRevieweeId() {
        return revieweeId;
    }

    public void setRevieweeId(Long revieweeId) {
        this.revieweeId = revieweeId;
    }

    public Long getReviewId() {
        return reviewId;
    }

    public void setReviewId(Long reviewId) {
        this.reviewId = reviewId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public byte getStars() {
        return stars;
    }

    public void setStars(byte stars) {
        this.stars = stars;
    }

    public UserModel getReviewer() {
        return reviewer;
    }

    public void setReviewer(UserModel reviewer) {
        this.reviewer = reviewer;
    }

    public UserModel getReviewee() {
        return reviewee;
    }

    public void setReviewee(UserModel reviewee) {
        this.reviewee = reviewee;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }
}
