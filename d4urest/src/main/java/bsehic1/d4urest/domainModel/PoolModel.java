package bsehic1.d4urest.domainModel;

import bsehic1.d4urest.dto.Pool;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "pools")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) //Serialization happens before lazy loaded objects are loaded
public class PoolModel implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long poolId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = UserModel.class)
    @JoinColumn(name = "createdById")
    private UserModel createdBy;

    @Column(name = "createdById", insertable = false, updatable = false)
    private Long createdById;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = UserModel.class)
    @JoinColumn(name = "passengerId")
    private UserModel passenger;

    @Column(name = "passengerId", insertable = false, updatable = false)
    private Long passengerId;

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = DriveModel.class)
    @JoinColumn(name = "driveId")
    private DriveModel drive;

    @Column(name = "driveId", insertable = false, updatable = false)
    private Long driveId;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public PoolModel(){
        
    }

    public PoolModel(Long poolId,Date created, Date updated, Long createdById, Long passengerId, Long driveId) {
        this.poolId = poolId;
        this.created = created;
        this.updated = updated;
        this.createdById = createdById;
        this.passengerId = passengerId;
        this.driveId = driveId;
    }

    public PoolModel(Pool pool){
        this.poolId = pool.getPoolId();
        this.created = pool.getCreated();
        this.updated = pool.getUpdated();
        this.createdById = pool.getCreatedById();
        this.passengerId = pool.getPassengerId();
        this.driveId = pool.getDriveId();
    }

    public Pool toPool(){
        return new Pool(this);
    }

    public Long getPoolId() {
        return this.poolId;
    }

    public void setPoolId(Long poolId) {
        this.poolId = poolId;
    }

    public Date getCreated() {
        return this.created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public UserModel getCreatedBy() {
        return this.createdBy;
    }

    public void setCreatedBy(UserModel createdBy) {
        this.createdBy = createdBy;
    }

    public Long getCreatedById() {
        return this.createdById;
    }

    public void setCreatedById(Long createdById) {
        this.createdById = createdById;
    }

    public DriveModel getDrive() {
        return this.drive;
    }

    public void setDrive(DriveModel drive) {
        this.drive = drive;
    }

    public Long getDriveId() {
        return this.driveId;
    }

    public void setDriveId(Long driveId) {
        this.driveId = driveId;
    }

    public UserModel getPassenger() {
        return this.passenger;
    }

    public void setPassenger(UserModel passenger) {
        this.passenger = passenger;
    }

    public Long getPassengerId() {
        return this.passengerId;
    }

    public void setPassengerId(Long passengerId) {
        this.passengerId = passengerId;
    }

}
