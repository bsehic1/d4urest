package bsehic1.d4urest.domainModel;

import bsehic1.d4urest.dto.Location;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "locations")
@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"}) //Serialization happens before lazy loaded objects are loaded
public class LocationModel implements Serializable{

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long locationId;

    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @NotNull
    private double longitude;

    @NotNull
    private double latitude;

    private String title;

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = RouteModel.class)
    @JoinColumn(name = "routeId")
    private RouteModel route;

    @Column(name = "routeId", insertable = false, updatable = false)
    private Long routeId;

    @PrePersist
    protected void onCreate() {
        created = new Date();
    }

    @PreUpdate
    protected void onUpdate() {
        updated = new Date();
    }

    public LocationModel() {
    }

    public LocationModel(Location location){
        this.locationId = location.getLocationId();
        this.created = location.getCreated();
        this.updated = location.getUpdated();
        this.latitude = location.getLatitude();
        this.longitude = location.getLongitude();
        this.title = location.getTitle();
        this.routeId = location.getRouteId();
    }

    public LocationModel(Long locationId, Date created, Date updated, @NotNull double longitude, @NotNull double latitude, String title, Long routeId) {
        this.locationId = locationId;
        this.created = created;
        this.updated = updated;
        this.longitude = longitude;
        this.latitude = latitude;
        this.title = title;
        this.routeId = routeId;
    }

    public Location toLocation(){
        return new Location(this);
    }

    public Long getRouteId() {
        return routeId;
    }

    public void setRouteId(Long routeId) {
        this.routeId = routeId;
    }

    public Long getLocationId() {
        return locationId;
    }

    public void setLocationId(Long locationId) {
        this.locationId = locationId;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public RouteModel getRoute() {
        return route;
    }

    public void setRoute(RouteModel route) {
        this.route = route;
    }

    public Date getCreated() {
        return created;
    }

    public Date getUpdated() {
        return updated;
    }
}
